import React from 'react';
import {graphql} from 'gatsby';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import Layout from '../components/layout';
import SEO from '../components/seo';
import ProductsCategoryList from '../components/productsCategoryList';

const IndexPage = ({data}) => {

  return (
      <Layout>
        <SEO title="Home"/>
        <section className="page__section main" color="cloudy-knoxville-gradient">
          <div className="container">
            <div className="row d-flex flex-column flex-md-row">
              <div className="col-12 col-md-8 main__cafe d-flex flex-column justify-content-center">
                <h1>Братец кролик</h1>
                <p>Кафе в английском стиле  </p>
              </div>
              <div className="col-12 col-md-4 main__map p-0">
                <iframe
                  src="https://yandex.ru/map-widget/v1/?um=constructor%3Ad40c1175b2ca3cf265d353126b5f13a8ea6baf3fbdbbc9d31d553c35c326dbec&amp;source=constructor"
                  width="100%" height="300" frameBorder="0" title="Братец Кроли на карте!"/>
              </div>
            </div>
          </div>
        </section>
        <section className="page__section">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <ProductsCategoryList categoryProducts={data.allStrapiCategories.nodes}/>
              </div>
            </div>
          </div>
        </section>
      </Layout>
  )
}

export const query = graphql`{
  allStrapiCategories {
    nodes {
      strapiId
      slug
      title
      goods {
        id
        title
        quantity
        price
        goods_imgs {
          url
        }
      }
    }
  }
}`;



export default IndexPage;
