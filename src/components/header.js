import React from 'react';
import {MDBBadge, MDBBtn, MDBIcon, MDBPopover} from 'mdbreact';
import { Link } from 'gatsby';
import Logo from '../static/svg/krolik.svg'
import Navigation from './nav';
import {connect} from 'react-redux';
import CartView from './cartView';

class Header extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      products: this.props.product_cart,
      siteTitle: this.props.siteTitle,
      product_cart: this.props.product_cart ? this.props.product_cart.length : 0,
      categories: this.props.categories
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if( nextProps.product_cart !== nextContext.product_cart){
      this.setState({product_cart: nextProps.product_cart.length})
      this.setState({products: nextProps.product_cart})
    }
    return null
  }

  render(){
    const siteTitle = this.state.siteTitle, count = this.state.product_cart, categories = this.state.categories;
    return (
      <header className="page__header header">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex align-items-center justify-content-between pt-2">
              <div className="header__logo logo d-flex align-items-center">
                <Link to="/">
                  <img className="logo__image" src={Logo} alt=""/>
                </Link>
                <div className="logo__text d-flex flex-column">
                  <h5 className="logo__heading h5-responsive m-0">{siteTitle}</h5>
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div className="header__cart mr-1">
                  <MDBPopover placement="bottom" popover clickable id="popovermobile">
                    <MDBBtn className="d-flex d-md-none align-items-center justify-content-center count__btn--mobile">
                      <MDBIcon icon="shopping-cart" />
                      <MDBBadge pill color="dark">{count}</MDBBadge>
                    </MDBBtn>
                    <div>
                      <CartView product_cart={this.props.product_cart}/>
                    </div>
                  </MDBPopover>
                  <MDBPopover placement="bottom" popover clickable id="popoverdesktop">
                    <MDBBtn gradient={"juicy-peach"} className="d-none d-md-flex align-items-center justify-content-center mr-2 count__btn">
                      <MDBIcon icon="shopping-cart" className="mr-2"/>
                      <p className="mb-0">Корзина: {count} товаров</p>
                    </MDBBtn>
                    <div>
                      <CartView product_cart={this.state.products}/>
                    </div>
                  </MDBPopover>
                </div>
                <div className="header__contacts d-flex flex-column">
                  <a className="header__phone m-0 d-none d-md-flex" href="tel:(89588316930)">8 (958) 831-69-30</a>
                  <p className="d-none d-md-flex m-0">ул. Кафейная д.18</p>
                  <a className="header__phone--mobile m-0 d-flex d-md-none btn" href="tel:(89588316930)">
                    <MDBIcon icon="phone"/>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 px-0 pt-2">
              <Navigation categories={categories}/>
            </div>
          </div>
        </div>
      </header>
    )
  }
}

const mapStateToProps = ({ product_cart }) => {
  return { product_cart }
}

export default connect(mapStateToProps)(Header);
