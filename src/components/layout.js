import React from 'react';
import PropTypes from 'prop-types';
import {useStaticQuery, graphql} from 'gatsby';

import Header from './header';
import './layout.css';

const Layout = ({children}) => {
  const data = useStaticQuery(graphql`
    query SiteBaseData {
      site {
        siteMetadata {
          title
        }
      }
      allStrapiCategories {
        nodes {
          strapiId
          slug
          title
        }
      }
    }
  `);

  return (
    <>
      <Header siteTitle={data.site.siteMetadata.title} categories={data.allStrapiCategories.nodes}/>
      {children}
      <footer>
        <div className="container">
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
        </div>
      </footer>
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
