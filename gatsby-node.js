exports.createPages = async function({ actions, graphql }) {
  const { data } = await graphql(`
    query {
       allStrapiCategories {
          nodes {
            slug
            goods {
              id
            }
          }
        }
    }
  `)
  data.allStrapiCategories.nodes.forEach(node => {
    const slug = node.slug;
    actions.createPage({
      path: `/${slug}`,
      component: require.resolve(`./src/templates/productTemplate.js`),
      context: { slug: slug },
    })
  })
}
